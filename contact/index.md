---
title: Contact Information
layout: default
---

I would love to set up a free 20 minute consultation to see if I will be a good fit for your needs. Please leave me your name, number, and a good time to reach you.

<table>
<tr><th>Email</th><td><a href="mailto:katie@therapysparrow.com">katie@therapysparrow.com</a></td></tr>
<tr><th>Phone</th><td>206-482-4951</td></tr>
</table>