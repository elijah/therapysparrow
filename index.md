---
title: Welcome to my practice!
layout: default
header_class: large
---

Life can go sideways in all kinds of ways, and therapy can give you concrete tools and aha moments that can change everything. My work focuses on deepening how we tell stories about ourselves and our lives, as well as understanding and healing the wounds we carry. I actively cultivate an atmosphere that welcomes and celebrates diversity of all kinds (race, sexual orientation, gender, class, age, ability, religion, spirituality). I approach therapy with empathy, honesty, humor, and a belief in healing and transformation. 

<img align="right" src="/katie-round-small-3.png" style="padding: 1em 0 1em 1em;">

In my practice, I see adults, teens, and couples. Coping with chronic illness, parenting, divorce, anxiety, grief and loss, and relationship troubles are particularly good matches for me. 

Besides being a therapist, I am a young adult scifi/fantasy writer, a parent, and have a daily mindfulness practice. I have greatly benefited from psychotherapy myself. 

I look forward to connecting with you.<br>

Katie Sparrow, LMFTA

**Credentials**

* Masters in Couple and Family Therapy, Antioch University 
* American Association of Marriage and Family Therapists (AAMFT) member
