---
title: Services
layout: default
---

## Individual Therapy for Adults

I work with adults experiencing a wide range of issues, and use evidence-based practices to customize therapy for each person depending on their values and goals. My specialties include chronic illness, divorce, parenting, anxiety, trauma, and grief. I work with people from many different backgrounds in an affirming way that respects people’s identities. 

## Individual Therapy for Teens

The hard work of transitioning from being a kid to an adult can be rocky, confusing, difficult, and strange. I work with teens to explore and affirm their emerging identities, strengthen their hopes and dreams, and create a safe place to explore all aspects of who they are and who they want to become.  

## Couples Therapy

My work with couples focuses on getting your deep needs met out of your relationship by increasing communication, insight, and intimacy. Some issues I work with in couples therapy include work/life balance, parenting, divorce, infidelity, intimacy, non-monogamy, and sex. I generally use Emotionally Focused Therapy for couples. 

## Fees

For a 50 minute session, I charge $165 for individuals and $185 for couples. I have some sliding scale availability.

I do not take insurance, however I can provide you with a monthly invoice to submit for reimbursement if your insurance allows.

For therapy to be most effective, sessions are typically at least once a week. This helps build momentum and allows us to create a strong working relationship. I have a 24-hour cancellation policy.

## Free Consultation

Before beginning therapy, I offer a free 20-minute phone consultation. I look forward to talking with you.
